# configureUnattendedUpgrades

'configureunattendedupgrades' is a simple script to enable unattended upgrades in Debian 10 and configure the system to install updates when they become available and reboot if necessary. All this script does is set up some configuration files. 