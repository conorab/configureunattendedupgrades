# Change Log

## Version 3.1-1

	gpg: assuming signed data in 'configureunattendedupgrades_3.1-1_all.deb'
	gpg: Signature made Sun 23 Aug 2020 22:49:32 AEST
	gpg:                using RSA key 2C3EBF239A279270E5399EF65DE5F96B92FA7361
	gpg: Good signature from "Conor Andrew Buckley" [ultimate]
	gpg:                 aka "[jpeg image of size 5400]" [ultimate]

	configureunattendedupgrades (3.1-1) unstable; urgency=medium

	  * Added copyright notice to top of script.

	 -- Conor Andrew Buckley <noreply@conorab.com>  Sun, 23 Aug 2020 22:18:00 +1000

Latest version according to www.conorab.com: 2020-08-23-22-51

## Version 1

	gpg: Signature made Sun 15 Dec 2019 22:44:56 AEDT
	gpg:                using RSA key E4F7B1B5A97074A0
	gpg: Good signature from "Conor Andrew Buckley - Archive ES 1 (Used to sign and encrypt files which will not be changed.)" [ultimate]
